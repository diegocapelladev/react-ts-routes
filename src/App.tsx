import { Navbar } from './components/Navbar'
import { Router } from './Routes'

export function App() {
  return (
    <>
      <Navbar />
      <Router />
    </>
  )
}
