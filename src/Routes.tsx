import { Navigate, Route, Routes } from 'react-router-dom'
import { Login } from './components/Login'
import { NotFound } from './components/NotFound'
import { Production } from './components/Production'
import { ProtectedRoute } from './components/ProtectedRoute'
import { Recipe } from './components/Recipe'
import { Stock } from './components/Stock'

export const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/production" />} />

      <Route path="/production" element={<Production />}>
        <Route path=":selectedProduct" element={<Recipe />} />
      </Route>

      <Route
        path="/stock"
        element={
          <ProtectedRoute>
            <Stock />
          </ProtectedRoute>
        }
      />

      <Route path="/login" element={<Login />} />

      <Route path="*" element={<NotFound />} />
    </Routes>
  )
}
