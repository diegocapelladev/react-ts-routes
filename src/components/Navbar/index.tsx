import * as S from './styles'

export const Navbar = () => (
  <S.Wrapper>
    <S.Link to="/production">Production</S.Link>
    <S.Link to="/stock">Stock</S.Link>
  </S.Wrapper>
)
