import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 6rem;
  background-color: #f7f3e3;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
`

export const Link = styled(NavLink)`
  text-decoration: none;
  color: black;
  font-size: 1.5rem;

  &.active {
    color: #6f1a07;
    border: 1px solid #af9164;
  }
`
