import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;

  button {
    background: teal;
    color: white;
    padding: 1rem;
    margin-top: 4rem;
    font-size: 2rem;
  }
`
