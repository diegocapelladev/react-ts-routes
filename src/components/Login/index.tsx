import { useNavigate } from 'react-router-dom'

import * as S from './styles'

export const Login = () => {
  const navigate = useNavigate()

  function handleLogin() {
    localStorage.setItem('token', 'a1b2c3d4')
    navigate('/stock')
  }

  return (
    <S.Wrapper>
      <button onClick={handleLogin}>Login</button>
    </S.Wrapper>
  )
}
