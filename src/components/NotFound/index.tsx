import * as S from './styles'

export const NotFound = () => (
  <S.Wrapper>
    <h1>Ops... Não encontri o que voçê queria!</h1>
    <img
      src="https://images.unsplash.com/photo-1589652717521-10c0d092dea9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1470&q=80"
      alt=""
    />
  </S.Wrapper>
)
