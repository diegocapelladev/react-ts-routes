import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 20rem;
  gap: 4rem;

  h3 {
    margin-top: 4rem;
    font-size: ${({ theme }) => theme.font.sizes.size32PX};
  }
`

export const ProductsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  background: #fafafa;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
  width: 100%;
`

export const ProductImage = styled.img`
  width: 32rem;
  border-radius: 5px;
`

export const Product = styled(Link)`
  padding: 3rem 0rem;
  cursor: pointer;
  text-decoration: none;
`
