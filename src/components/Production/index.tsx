import { Outlet } from 'react-router-dom'

import data from '../../assets/data.json'

import * as S from './styles'

export const Production = () => {
  return (
    <S.Wrapper>
      <h3>O que você vai fabricar hoje?</h3>

      <S.ProductsContainer>
        {data.products.map((product) => (
          <S.Product key={product.id} to={`./${product.id}`}>
            <S.ProductImage src={product.image} />
            <h4>{product.name}</h4>
          </S.Product>
        ))}
      </S.ProductsContainer>

      <Outlet />
    </S.Wrapper>
  )
}
