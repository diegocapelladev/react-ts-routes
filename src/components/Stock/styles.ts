import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 2rem;
`

export const ShowStock = styled.div`
  display: flex;
  align-items: center;
  gap: 2rem;
`

export const StockContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  background-color: #fafafa;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
  padding: 2rem 0;
`
export const ButtonVisibility = styled.button`
  border: none;
  background: transparent;
  cursor: pointer;

  svg {
    font-size: 2rem;
  }
`
