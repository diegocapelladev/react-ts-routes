import { useLocation, useNavigate } from 'react-router-dom'
import { AiFillEyeInvisible, AiFillEye } from 'react-icons/ai'

import data from '../../assets/data.json'

import * as S from './styles'

export const Stock = () => {
  const navigate = useNavigate()

  const location = useLocation()
  const queryParam = new URLSearchParams(location.search)

  const showStock = queryParam.get('showStock') === 'true'

  function handleToggleShowStock() {
    navigate(`?showStock=${!showStock}`, { replace: true })
  }

  return (
    <S.Wrapper>
      <S.ShowStock>
        <h3>Stock of Products</h3>
        <S.ButtonVisibility onClick={handleToggleShowStock}>
          {showStock ? <AiFillEyeInvisible /> : <AiFillEye />}
        </S.ButtonVisibility>
      </S.ShowStock>

      <S.StockContainer>
        {data.products.map((product) => (
          <div key={product.id}>
            <h4>{product.name}</h4>
            <h5>{showStock ? product.stock : '-'}</h5>
          </div>
        ))}
      </S.StockContainer>
    </S.Wrapper>
  )
}
